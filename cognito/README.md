# @ngauth/cognito

## Overview

`NgAuth` is a suite of Angular modules
comprising a set of auth-related reusable components and services.
The library is mostly frontend/UI, and it uses AWS Cognito for serverless backend.
The `NgAuth/Cognito` module provide a concrete auth service provider implemenation for `@ngauth/services`.
It uses AWS Cognito User Pool as an identity provider.



_(Note: this library is currently in beta,_
_and the APIs may change over time.)_



## Installation

To install this library, run:

```bash
$ npm install --save @ngauth/cognito @ngauth/services @ngauth/core
```


Note that NgAuth requires at least one concrete backend auth service provider.
`@ngauth/cognito` is one such implementation.
You will need to inject the cognito service provider (via the `factory` mechanism)
in your app (in your main `NgModule`).




## Using the library

From your Angular `AppModule`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Import the library
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';
import { NgAuthCognitoModule } from '@ngauth/cognito';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Specify the library as an import
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
    NgAuthCognitoModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Once the library is imported, you can use its components 
and services in your Angular application:



## License

MIT © [Harry Y](mailto:sidewaybot@gmail.com)

import { Injectable } from "@angular/core";
import { AuthenticationDetails, CognitoUser } from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";
import * as STS from "aws-sdk/clients/sts";
import { Observable } from 'rxjs/Observable';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { LoggedInStatus, CognitoResult, NewPasswordInfo } from '@ngauth/core';
import { CognitoCallback, LoggedInCallback } from '@ngauth/core';
import { IUserLoginService } from '@ngauth/services';
import { CognitoCommonUtil } from './cognito-common-util';
import { DdbServiceUtil } from './ddb-service-util';
import { UserLoginHelper } from './user-login-helper';



@Injectable()
export class CognitoUserLoginService implements IUserLoginService {

  constructor(
    private cognitoUtil: CognitoCommonUtil,
    private ddbUtil: DdbServiceUtil,
    private userLoginHelper: UserLoginHelper
  ) {
  }

  public isAuthenticated(): Observable<LoggedInStatus> {
    return Observable.create((obs) => {
      this.userLoginHelper.isAuthenticated({
        isLoggedIn: (message: string, loggedIn: boolean) => {
          let loggedInStatus: LoggedInStatus = {
            message: message,
            loggedIn: loggedIn
          };
          obs.next(loggedInStatus);
        }
      });
    });
  }

  public authenticate(username: string, password: string): Observable<CognitoResult> {
    return Observable.create((obs) => {
      this.userLoginHelper.authenticate(username, password, {
        cognitoCallback: (message: string, result: any) => {

          if(dl.isLoggable()) dl.log(`userLoginHelper.authenticate() called. message = ${message}; result = ${result}.`);
          let cognitoResult: CognitoResult = {
            message: message,
            result: result,
            extra: null
          };
          obs.next(cognitoResult);
        }
      });
    });
  }

  public startResetPassword(username: string): Observable<CognitoResult> {
    return Observable.create((obs) => {
      this.userLoginHelper.forgotPassword(username, {
        cognitoCallback: (message: string, result: any) => {
          let cognitoResult: CognitoResult = {
            message: message,
            result: result,
            extra: null
          };
          obs.next(cognitoResult);
        }
      });
    });
  }

  public confirmResetPassword(email: string, verificationCode: string, password: string): Observable<CognitoResult> {
    return Observable.create((obs) => {
      this.userLoginHelper.confirmNewPassword(email, verificationCode, password, {
        cognitoCallback: (message: string, result: any) => {
          let cognitoResult: CognitoResult = {
            message: message,
            result: result,
            extra: null
          };
          obs.next(cognitoResult);
        }
      });
    });
  }


  public changePassword(newPasswordInfo: NewPasswordInfo): Observable<CognitoResult> {
    return Observable.create((obs) => {
      this.userLoginHelper.changePassword(newPasswordInfo, {
        cognitoCallback: (message: string, result: any) => {
          let cognitoResult: CognitoResult = {
            message: message,
            result: result,
            extra: null
          };
          obs.next(cognitoResult);
        }
      });
    });
  }

  public logout(): void {
    this.userLoginHelper.logout();
  }
 
}

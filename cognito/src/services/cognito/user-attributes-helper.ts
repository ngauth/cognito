import { Injectable } from "@angular/core";

import { CommonCallback } from '@ngauth/core';
import { CognitoCommonUtil } from './cognito-common-util';


@Injectable()
export class UserAttributesHelper {

  constructor(private cognitoUtil: CognitoCommonUtil) {
  }

  public getAttributes(callback: CommonCallback) {
    let cognitoUser = this.cognitoUtil.getCurrentUser();

    if (cognitoUser != null) {
      cognitoUser.getSession(function (err, session) {
        if (err)
          console.log("UserAttributesHelper: Couldn't retrieve the user");
        else {
          cognitoUser.getUserAttributes(function (err, result) {
            if (err) {
              console.log("UserAttributesHelper: in getParameters: " + err);
            } else {
              callback.callbackWithParam(result);
            }
          });
        }

      });
    } else {
      callback.callbackWithParam(null);
    }


  }
}
import { Inject, Injectable } from "@angular/core";
import { AuthenticationDetails, CognitoUser, CognitoUserAttribute } from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";
import { Observable } from 'rxjs/Observable';

import { CognitoResult, UserRegistrationInfo, NewPasswordInfo } from '@ngauth/core';
import { CognitoCallback } from '@ngauth/core';
import { IUserRegistrationService } from '@ngauth/services';
import { CognitoCommonUtil } from './cognito-common-util';
import { UserRegistrationHelper } from './user-registration-helper';


@Injectable()
export class CognitoUserRegistrationService implements IUserRegistrationService {

  constructor(
    // private cognitoUtil: CognitoCommonUtil,
    private userRegistrationHelper: UserRegistrationHelper
  ) {
  }

  public register(user: UserRegistrationInfo): Observable<CognitoResult> {
    return Observable.create((obs) => {
      this.userRegistrationHelper.register(user, {
        cognitoCallback: (message: string, result: any) => {
          let cognitoResult: CognitoResult = {
            message: message,
            result: result,
            extra: null
          };
          obs.next(cognitoResult);
        }
      });
    });
  }

  public confirmRegistration(username: string, confirmationCode: string): Observable<CognitoResult> {
    return Observable.create((obs) => {
      this.userRegistrationHelper.confirmRegistration(username, confirmationCode, {
        cognitoCallback: (message: string, result: any) => {
          let cognitoResult: CognitoResult = {
            message: message,
            result: result,
            extra: null
          };
          obs.next(cognitoResult);
        }
      });
    });
  }

  public resendCode(username: string): Observable<CognitoResult> {
    return Observable.create((obs) => {
      this.userRegistrationHelper.resendCode(username, {
        cognitoCallback: (message: string, result: any) => {
          let cognitoResult: CognitoResult = {
            message: message,
            result: result,
            extra: null
          };
          obs.next(cognitoResult);
        }
      });
    });
  }

  // public changePassword(newPasswordInfo: NewPasswordInfo): Observable<CognitoResult> {
  //   return Observable.create((obs) => {
  //     this.userRegistrationHelper.newPassword(newPasswordInfo, {
  //       cognitoCallback: (message: string, result: any) => {
  //         let cognitoResult: CognitoResult = {
  //           message: message,
  //           result: result,
  //           extra: null
  //         };
  //         obs.next(cognitoResult);
  //       }
  //     });
  //   });
  // }

}

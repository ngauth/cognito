import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';

import { CommonCallback } from '@ngauth/core';
import { IUserAttributesService } from '@ngauth/services';
import { CognitoCommonUtil } from './cognito-common-util';
import { UserAttributesHelper } from './user-attributes-helper';



@Injectable()
export class CognitoUserAttributesService implements IUserAttributesService {

  constructor(
    // private cognitoUtil: CognitoCommonUtil,
    private userAttributesHelper: UserAttributesHelper
  ) {
  }

  public getAttributes(): Observable<any> {
    return Observable.create((obs) => {
      this.userAttributesHelper.getAttributes({
        callback: () => {},
        callbackWithParam: (result: any) => {
          obs.next(result);
        }
      });
    });
  }

}

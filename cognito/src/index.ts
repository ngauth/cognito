import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RouterModule } from '@angular/router';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';

import { CognitoCommonUtil } from './services/cognito/cognito-common-util';
import { DdbServiceUtil } from './services/cognito/ddb-service-util';
import { AwsServiceHelper } from './services/cognito/aws-service-helper';
import { UserRegistrationHelper } from './services/cognito/user-registration-helper';
import { UserLoginHelper } from './services/cognito/user-login-helper';
import { UserAttributesHelper } from './services/cognito/user-attributes-helper';

import { CognitoUserAttributesService } from './services/cognito/user-attributes.service';
import { CognitoUserRegistrationService } from './services/cognito/user-registration.service';
import { CognitoUserLoginService } from './services/cognito/user-login.service';
import { CognitoUserAuthServiceFactory } from './services/factory/cognito-user-auth-service-factory';

// import { SecureHomeComponent } from './secure/landing/secure-home.component';
// import { JwtDisplayComponent } from './secure/jwttokens/jwt-display.component';
// import { UserActivityComponent } from './secure/activity/user-activity.component';
// import { UserProfileComponent } from './secure/profile/user-profile.component';

export * from './services/cognito/common/aws-cognito-util';

export * from './services/cognito/cognito-common-util';
export * from './services/cognito/ddb-service-util';
export * from './services/cognito/aws-service-helper';
export * from './services/cognito/user-registration-helper';
export * from './services/cognito/user-login-helper';
export * from './services/cognito/user-attributes-helper';

export * from './services/cognito/user-attributes.service';
export * from './services/cognito/user-registration.service';
export * from './services/cognito/user-login.service';
export * from './services/factory/cognito-user-auth-service-factory';

// export * from './secure/common/token-callbacks';
// export * from './secure/common/attribute-callbacks';
// export * from './secure/landing/secure-home.component';
// export * from './secure/jwttokens/jwt-display.component';
// export * from './secure/activity/user-activity.component';
// export * from './secure/profile/user-profile.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    // RouterModule,
    // // RouterModule.forRoot([{ path: '', component: SecureHomeComponent }]),
    NgCoreCoreModule.forRoot(),
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class NgAuthCognitoModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgAuthCognitoModule,
      providers: [
        CognitoCommonUtil,
        DdbServiceUtil,
        AwsServiceHelper,
        UserRegistrationHelper,
        UserLoginHelper,
        UserAttributesHelper,
        CognitoUserAttributesService,
        CognitoUserRegistrationService,
        CognitoUserLoginService,
        CognitoUserAuthServiceFactory
        // UserAuthServiceFactory,
        // { provide: UserAuthServiceFactory, useClass: CognitoUserAuthServiceFactory }
      ]
    };
  }
}
